from django.http import JsonResponse


def ping(request):
    data = {"ping": "pong!", "updated": "yes"}
    return JsonResponse(data)
